#!/bin/bash

# -e causes the shell to exit if any subcommand or pipeline returns a non-zero status
set -e

find src/test/resources/test-input/feature/ -name "*.odt" -exec ./create-single-test-coverage.sh {} \;
    # {} is the found file path of the test odt and \; will provide one name each for the bash
