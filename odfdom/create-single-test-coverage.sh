#!/bin/bash
# -e causes the shell to exit if any subcommand or pipeline returns a non-zero status
set -e

# !!! PLEASE UPDATE VARIABLES BELOW!!!
if (( $# >= 1 )); then
        # ODT feature document trunc name, e.g src/test/resources/test-input/feature/
        one=${1##*/} # Taking string from first parameter and removing longest substring from the right ## till /
        ODF_FILE_NAME__TRUNC=${one%.odt*} # removing .odt suffix so only the trunc of the filename remains
else
        ODF_FILE_NAME__TRUNC="table_merged-cells" # default if no parameter was given
fi
# Version number of the Jacoco release being used for testing, required to identify its JAR
JACOCO_AGENT_VERION="0.8.8"
# Maven .m2 repository parent directory, required to access the JaCoCo JAR
MAVEN_REPO_PATH=$HOME/.m2
# Version nmber of the ODFDOM being used
ODFDOM_VERSION=0.10.0-SNAPSHOT

echo "*** Test file trunc name:'$ODF_FILE_NAME__TRUNC'"

# DO NOT CHANGE BELOW THE FOLLOWING LINE
# --------------------------------------------------------

# -f gives no error if no jacoco.exec exists
rm -f ./target/jacoco.exec
rm -rf ./target/site

# Triggers a single test 'FeatureLoadTest' with the system property 'textFeatureName' being the trunc name of an ODT test file
# being loaded from src/test/resources/test-input/feature/
mvn surefire:test -Dtest=FeatureLoadTest -DtextFeatureName=${ODF_FILE_NAME__TRUNC} -DargLine=-javaagent:$MAVEN_REPO_PATH/repository/org/jacoco/org.jacoco.agent/${JACOCO_AGENT_VERION}/org.jacoco.agent-${JACOCO_AGENT_VERION}-runtime.jar=destfile=./target/jacoco.exec

# Creates the JaCoco XML file and HTML coverage report from the binary target/jacoco.exec file
mvn jacoco:report

# Save the Jacoco XML file using the cov suffix to avoid whitespace lint on XML
cp ./target/site/jacoco/jacoco.xml src/test/resources/test-input/feature/coverage/jacoco_${ODF_FILE_NAME__TRUNC}.cov.xml

# XML pretty-printing/indentation - xmllint comes with libxml2-utils: https://gitlab.gnome.org/GNOME/libxml2
xmllint --format src/test/resources/test-input/feature/coverage/jacoco_${ODF_FILE_NAME__TRUNC}.cov.xml > src/test/resources/test-reference/feature/coverage/jacoco_${ODF_FILE_NAME__TRUNC}_indent.cov.xml

# Transform the jacoco XML to cobertura xml file and keeping it using the cov suffix to avoid whitespace lint on XML
python2 cover2cover.py target/site/jacoco/jacoco.xml src/main/java > src/test/resources/test-input/feature/coverage/cobertura_${ODF_FILE_NAME__TRUNC}.cov.xml

# XML pretty-printing/indentation - xmllint comes with libxml2-utils: https://gitlab.gnome.org/GNOME/libxml2
xmllint --format src/test/resources/test-input/feature/coverage/cobertura_${ODF_FILE_NAME__TRUNC}.cov.xml > src/test/resources/test-reference/feature/coverage/cobertura_${ODF_FILE_NAME__TRUNC}_indent.cov.xml

# Strip the uncovered lines from the Cobertura Coverage XML
# java  org.odftoolkit.odfdom.changes.CoberturaXMLHandler -cp ./target/odfdom-java-${ODFDOM_VERSION}-jar-with-dependencies.jar  ./src/test/resources/test-input/feature/coverage/cobertura_${ODF_FILE_NAME__TRUNC}.cov.xml
###java -cp ./target/odfdom-java-${ODFDOM_VERSION}-jar-with-dependencies.jar org.odftoolkit.odfdom.changes.CoberturaXMLHandler ./cobertura_${ODF_FILE_NAME__TRUNC}.cov.xml

###cp target/test-classes/test-reference/feature/coverage/cobertura_${ODF_FILE_NAME__TRUNC}_stripped.cov.xml src/test/resources/test-reference/feature/coverage/cobertura_${ODF_FILE_NAME__TRUNC}_stripped.cov.xml

# XML pretty-printing/indentation - xmllint comes with libxml2-utils: https://gitlab.gnome.org/GNOME/libxml2
###xmllint --format src/test/resources/test-reference/feature/coverage/cobertura_${ODF_FILE_NAME__TRUNC}_stripped.cov.xml > src/test/resources/test-reference/feature/coverage/cobertura_${ODF_FILE_NAME__TRUNC}_stripped-indent.cov.xml
